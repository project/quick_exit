/**
 * @file
 * quick_exit behaviors.
 */
(function (Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.quickExitQuickExit = {
    attach (context, settings) {
      const quickExit = once('quickExitQuickExit', '.quick-exit');
      const quickExitUrls = drupalSettings.quick_exit.exit_urls?.length ? drupalSettings.quick_exit.exit_urls : ['https://www.google.com'];
      if (quickExit.length > 0) {
        quickExit.forEach(el => el.addEventListener('click', function (e) {
          e.preventDefault();
          // Open a new tab with a distraction page
          let randomIndex = Math.floor(Math.random() * quickExitUrls.length);
          const quickExitUrl = quickExitUrls[randomIndex];
          window.open(quickExitUrl, '_blank');
          
          // Replace the current tab with a distraction page
          if (randomIndex !== drupalSettings.quick_exit.exit_urls.length - 1) {
            randomIndex += 1;
          } else {
            randomIndex = 0;
          }
          const quickExitUrl2 = quickExitUrls[randomIndex];
          window.location.replace(quickExitUrl2);

          // Optionally, clear browsing history to further enhance privacy
          if (window.history && window.history.replaceState) {
            window.history.replaceState({}, document.title, window.location.href);
          }
        }));
      }
    }
  };

} (Drupal, drupalSettings));
