How it Works
The Quick_Exit module adds an action that moves a visitor off the current site in case of an emergency. When the button is pressed, the visitor leaves the current site and the browser history from your site’s page is scrubbed. The site they are taken to is configurable, but common destinations include YouTube, Netflix, or Wordle (New York Times).

Alternate names for the quick exit button could include but is not limited to: quick exit, exit site, leave site, safety exit

How to Install
Follow standard Drupal module install process.

Settings:
Go to Configuration > Quick Exit Settings
Edit URLs (Note: If you only have one URL it will load that URL twice, otherwise it will pull from this list at random.)


Disclaimer & More Research
Currently, there is no “‘correct”’ way to implement an easy exit. This module is one method that follows our own findings for successful use, but the pattern still requires further research. For more information on methods used, we recommend reading Click Here to Exit: An Evaluation of Quick Exit Buttons by Kieron Ivy Turk and Alice Hutchings.

Current Limitations of Module
Technical
The browser history is scrubbed on the page the exit button is used on. Ideally, browser history for your entire site should be scrubbed. Browsing history for other sites should stay, as a lack of history can cause suspicion in those who might be monitoring the users' behavior 
A pop-up upon entry to the site can educate the audience about what the exit button does. However, on sites with a wide audience that isn’t just focused on sensitive topics, it is best to compare and contrast the pros and cons of the additional popup to educate visitors (See “overusage” below).
 
Philosophical
Learning/ Education of exit button: The quick exit button is not something that is widely known to society. Those who are in more marginalized groups, such as domestic violence victims, and LGBTQIA+ members, may have received education about the presense of and how to use similarthe actionsbutton or its existence. Therefore, the quick exit button's effectivenessefficiency at the moment is unknown. 
Overusage. If this module is used in places where it is unnecessaryfor almost every website, regardless of audience it may help inform the wrong audience, such as the abuser. If the victim is using a website with a quick exit button, and the abuser catches them on the site, they may become suspicious that the site they are on has a button in the first place.

Feedback
We would love to continue to make improvements to this module. If you download and use this module, and have feedback about how users have interacted with it please reach out to Alyssa at @avarsanyi 

