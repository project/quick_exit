<?php

declare(strict_types=1);

namespace Drupal\quick_exit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure quick_exit settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'quick_exit_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['quick_exit.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $urls = $this->config('quick_exit.settings')->get('exit_urls') ?? [];

    if (empty($urls)) {
      $urls = [''];
    }

    $form['#tree'] = TRUE;
    $form['exit_urls'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Exit URLs'),
      '#description' => $this->t('The URLs to redirect to when the user clicks exit site. To add a new URL, enter it in the last textfield.'),
      '#description_display' => 'before',
      '#prefix' => '<div id="exit-urls-wrapper">',
      '#suffix' => '</div>',

    ];

    $stateCount = $form_state->get('num_exit_urls');
    if ($stateCount === NULL) {
      $form_state->set('num_exit_urls', count($urls));
      $stateCount = count($urls);
    }

    for ($i = 0; $i < $stateCount; $i++) {
      $form['exit_urls'][] = [
        '#type' => 'textfield',
        '#default_value' => $urls[$i] ?? '',
      ];
    }

    $form['exit_urls'][] = [
      '#type' => 'textfield',
      '#default_value' => '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Validate url.
    $urls = $form_state->getValue('exit_urls');

    foreach ($urls as $key => $url) {
      if (!empty($url) && !filter_var($url, FILTER_VALIDATE_URL)) {
        $form_state->setErrorByName('exit_urls', $this->t('The URL is not valid.')->__toString());
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $urls = $form_state->getValue('exit_urls');

    // Remove empty values.
    $urls = array_filter($urls, function ($url) {
      return !empty($url);
    });

    $this->config('quick_exit.settings')
      ->set('exit_urls', array_values($urls))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
